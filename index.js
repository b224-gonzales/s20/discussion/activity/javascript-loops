
let givenNum = Number(prompt("Input a number: "));
console.log("The number you provided is " + givenNum + ".");

for (givenNum; givenNum >= 0; givenNum--) {
  
  if (givenNum <= 50) {
    console.log(
      "The current value is at " + givenNum + ". Terminating the loop."
    );
    break;
  }

  if (givenNum % 10 === 0) {
    console.log("The number is divisible by 10. Skipping the number.");
    continue;
  }

  if (givenNum % 5 === 0) {
    console.log(givenNum);
    continue;
  }
}

// supercalifragilisticexpialidocious


let longWord = "supercalifragilisticexpialidocious";
let consonants = " ";

for (let i = 0; i < longWord.length; i++) {
  if (
    longWord[i].toLowerCase() == "a" ||
    longWord[i].toLowerCase() == "e" ||
    longWord[i].toLowerCase() == "i" ||
    longWord[i].toLowerCase() == "o" ||
    longWord[i].toLowerCase() == "u"
  ) {
    continue;
  } else {
    consonants += longWord[i];
  }
}

console.log(longWord);
console.log(consonants);
